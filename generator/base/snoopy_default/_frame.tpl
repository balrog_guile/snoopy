<!DOCTYPE HTML>
<!--
	Strongly Typed 1.1 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Right Sidebar - Strongly Typed by HTML5 UP</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="viewport" content="width=1040" />
		<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600|Arvo:700" rel="stylesheet" type="text/css" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
	</head>
	<body class="right-sidebar">

		<!-- Header Wrapper -->
			<div id="header-wrapper">
						
				<!-- Header -->
					<div id="header" class="container">
						
						<!-- Logo -->
							<h1 id="logo"><a href="#">{$title}</a></h1>
							<p>{$subtitle}</p>
						
						<!-- Nav -->
							<nav id="nav">
								{include file="_header_navi.tpl"}
							</nav>

					</div>

			</div>
			
		<!-- Main Wrapper -->
			<div id="main-wrapper">

				<!-- Main -->
					<div id="main" class="container">
						<div class="row">
						
							<!-- Content -->
								<div id="content" class="8u skel-cell-important">

									<!-- Post -->
										<article class="is-post">
											{$post}
										</article>
								
								</div>
								
							<!-- Sidebar -->
								<div id="sidebar" class="4u">
									{include file="_side_navi.tpl"}
								</div>

						</div>
					</div>

			</div>

		<!-- Footer Wrapper -->
			<div id="footer-wrapper">

				<!-- Footer -->
					<div id="footer" class="container">
						<header>
							<h2>Questions or comments? <strong>Get in touch:</strong></h2>
						</header>
						<div class="row">
							<div class="6u">
								<section>
									<form method="post" action="#">
										<div class="row half">
											<div class="6u">
												<input name="name" placeholder="Name" type="text" class="text" />
											</div>
											<div class="6u">
												<input name="email" placeholder="Email" type="text" class="text" />
											</div>
										</div>
										<div class="row half">
											<div class="12u">
												<textarea name="message" placeholder="Message"></textarea>
											</div>
										</div>
										<div class="row half">
											<div class="12u">
												<a href="#" class="button button-icon fa fa-envelope">Send Message</a>
											</div>
										</div>
									</form>
								</section>
							</div>
							<div class="6u">
								<section>
									<p>Erat lorem ipsum veroeros consequat magna tempus lorem ipsum consequat Phaselamet 
									mollis tortor congue. Sed quis mauris sit amet magna accumsan tristique. Curabitur 
									leo nibh, rutrum eu malesuada.</p>
									<div class="row">
										<ul class="icons 6u">
											<li class="fa fa-home">
												1234 Somewhere Road<br />
												Nashville, TN 00000<br />
												USA
											</li>
											<li class="fa fa-phone">
												(000) 000-0000
											</li>
											<li class="fa fa-envelope">
												<a href="#">info@untitled.tld</a>
											</li>
										</ul>
										<ul class="icons 6u">
											<li class="fa fa-twitter">
												<a href="http://twitter.com/n33co">@untitled-tld</a>
											</li>
											<li class="fa fa-dribbble">
												<a href="http://dribbble.com/n33">dribbble.com/untitled-tld</a>
											</li>
											<li class="fa fa-facebook">
												<a href="#">facebook.com/untitled-tld</a>
											</li>
											<li class="fa fa-google-plus">
												<a href="#">google.com/+untitled-tld</a>
											</li>
										</ul>
									</div>
								</section>
							</div>
						</div>
					</div>

				<!-- Copyright -->
					<div id="copyright" class="container">
						<ul class="links">
							<li>&copy; Untitled. All rights reserved</li>
							<li>Demo images: <a href="http://regularjane.deviantart.com/">regularjane</a></li>
							<li>Design: <a href="http://html5up.net/">HTML5 UP</a></li>
						</ul>
					</div>

			</div>

	</body>
</html>