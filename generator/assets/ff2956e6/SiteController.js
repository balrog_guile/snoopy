/* =============================================================================
 * サイトコントローラー
 * ========================================================================== */
$(document).ready(function(){
	
	$('.delete_btn').on(
		'click',
		function(e){
			var get_id = $(this).attr('data-id');
			var url = delete_url + '?id=' + get_id;
			location.href = url;
		}
	);
	
	$('.generate_btn').on(
		'click',
		function(e){
			var get_id = $(this).attr('data-id');
			var url = genarate_url + '?id=' + get_id;
			location.href = url;
		}
	);
	
});