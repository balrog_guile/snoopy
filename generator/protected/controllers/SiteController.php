<?php
Yii::import('application.vendor.*');
require_once 'Smarty.class.php';
require_once 'AssetsHelper.php';
require_once 'DirHelper.php';
require_once 'SiteGenerate.php';

class SiteController extends Controller
{
	
	protected $assets_helper;
	
	
	// ----------------------------------------------------
	
	/**
	 * コンストラクタ
	 */
	public function __construct($id, $module = null) {
		
		parent::__construct($id, $module);
		$this->assets_helper = new AssetsHelper( 'ControllerJS' );
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$data = array();
		
		
		$this->assets_helper->set_js(get_class() . '.js');
		$this->assets_helper->add_js( 'var delete_url = \'' . $this->createUrl( 'site/delete',array()) . '\';', CClientScript::POS_HEAD );
		$this->assets_helper->add_js( 'var genarate_url = \'' . $this->createUrl( 'site/generate',array()) . '\';', CClientScript::POS_HEAD );
		
		//セッションの使い方
		/*
		$name = Yii::app()->session['name'];
		$name ++ ;
		echo $name;
		Yii::app()->session['name'] = $name;
		*/
		
		//作成済みディレクトリ
		$dirs = glob(  Yii::app()->params['smarty']['template_dir'] . '/*' , GLOB_ONLYDIR );
		$data['dirs'] = array();
		foreach( $dirs as $dir )
		{
			$data['dirs'][] = basename($dir);
		}
		
		//メッセージ
		$data['message'] = Yii::app()->session['message'];
		Yii::app()->session['message'] = null;
		$data['error_message'] = Yii::app()->session['error_message'];
		Yii::app()->session['error_message'] = null;
		
		
		//モデル
		$data['model'] = new SiteForm();
		
		
		
		$this->render( 'index', $data );
	}
	
	// ----------------------------------------------------
	
	/**
	 * サイトジェネレート
	 */
	public function actionGenerate()
	{
		$id = rawurldecode($_GET['id']);
		$dir = Yii::app()->params['smarty']['template_dir'] . '/' . $id;
		
		$site = new SiteGenerate();
		
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 削除
	 */
	public function actionDelete()
	{
		$id = rawurldecode($_GET['id']);
		$dir = Yii::app()->params['smarty']['template_dir'] . '/' . $id;
		DirHelper::removeDir($dir);
		Yii::app()->session['message'] = 'サイトを削除しました';
		$this->redirect(array('index'));
		
	}
	
	// ----------------------------------------------------
	
	public function actionCreate()
	{
		$data = array();
		$data['model'] = new SiteForm();
		$this->performAjaxValidation($data['model']);
		
		if(isset($_POST['SiteForm']))
		{
			
			$dist_dir =
				Yii::app()->params['smarty']['template_dir'] .
				'/' .
				
				CHtml::encode( str_replace( '.', '', $_POST['SiteForm']['name'] ) )
				;
			
			
			
			$from_dir = 
				Yii::getPathOfAlias( 'webroot' ) .
				'/base/' .
				Yii::app()->params['snoopy']['new_copy_dir']
				;
			
			
			//echo $from_dir;
			
			//ディレクトリコピー
			DirHelper::copyRecursive( $from_dir, $dist_dir );
			
			Yii::app()->session['message'] = 'サイトを作成しました';
			$this->redirect(array('index'));
			return;
		
		}
		
		Yii::app()->session['error_message'] = 'サイト作成に失敗しました';
		$this->redirect(array('index'));
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * AJAXバリデータ
	 * @param type $model
	 */
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='site-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	// ----------------------------------------------------
	
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	// ----------------------------------------------------
}