<?php
/* =============================================================================
 * Smaty設定
 * ========================================================================== */
return array(
	
	//テンプレートディレクトリ
	'template_dir' => realpath( dirname( __FILE__ ) . '/../../../sites/' ),
	
	//コンパイルディレクトリ
	'compile_dir' => realpath( dirname( __FILE__ ) . '/../../../sites_c/' ),
	
	//追加プラグインディレクトリ
	'plugin_dir' => realpath( dirname( __FILE__ ) . '/../smarty_plugin/' ),
	
	//デリミタ（はじまり）
	'left_delimiter' => '{',
	
	//デリミタ（おわり）
	'right_delimiter' => ')',
	
);