<?php
/* =============================================================================
 * サイト設定フォーム
 * ========================================================================== */
class SiteForm extends CFormModel
{
	
	public $name;
	
	
	// ----------------------------------------------------
	
	/**
	 * バリデーションルール
	 */
	
	public function rules()
	{
		return array(
			array( 'name', 'required' ),
			array( 'name', 'check_dir'),
		);
	}
	
	// ----------------------------------------------------
	
	public function attributeLabels()
	{
		return array(
			'name' => 'サイト名',
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * バリデーションルール追加ディレクトリの存在
	 * 
	 */
	
	public function check_dir($attribute,$params)
	{
		$file = Yii::app()->params['smarty']['template_dir'] . '/' . Chtml::encode( str_replace( '.', '', $this->name ) );
		if(is_dir($file))
		{
			$this->addError('name','存在するサイト名です');
		}
	}
	
	// ----------------------------------------------------
}