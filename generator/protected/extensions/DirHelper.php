<?php
/* =============================================================================
 * ファイル・ディレクトリ操作ヘルパ
 * ========================================================================== */
class DirHelper
{
	// ----------------------------------------------------
	/**
	 * ディレクトリの再帰削除
	 */
	public static function removeDir( $dir ) {
		$cnt = 0;
		$handle = opendir($dir);
		if (!$handle) {
			return ;
		}
		while (false !== ($item = readdir($handle)))
		{
			if ($item === "." || $item === "..") {
				continue;
			}
			$path = $dir . DIRECTORY_SEPARATOR . $item;
			if (is_dir($path)) {
				// 再帰的に削除
				$cnt = $cnt + self::removeDir($path);
			}
			else {
				// ファイルを削除
				unlink($path);
			}
		}
		closedir($handle);
		
		// ディレクトリを削除
		if (!rmdir($dir)) {
			return ;
		}
		
	}
	// ----------------------------------------------------
	
	/**
	 * ディレクトリの再帰コピー
	 */
	public static function copyRecursive($src, $dst) {
		
		// In case you'd like to delete exsiting files, you should call rmdir_recursive..
		//if (file_exists($dst)) {
		//    rmdir_recursive($dst);
		//}
		
		if (is_dir($src)) {
			//echo 1;
			mkdir($dst);
			$files = scandir($src);
			foreach ($files as $file) {
				if (($file != ".") && ($file != "..")) {
					self::copyRecursive("$src/$file", "$dst/$file");
				}
			}
		}
		else if (file_exists($src))
		{
			//echo 2;
			copy($src, $dst);
		}
		
		
		//echo $src . 'dd';
	}
	// ----------------------------------------------------
}