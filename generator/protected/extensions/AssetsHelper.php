<?php
/* =============================================================================
 * アセット処理用ヘルパ
 * ========================================================================== */
class AssetsHelper
{
	//アセットパス
	public $asset_path;
	
	//アセットURL
	protected $_assetsUrl;
	
	// ----------------------------------------------------
	
	/**
	 * コンストラクタ
	 * @param string $name
	 */
	public function __construct( $name )
	{
		$this->asset_path = $name;
	}
	
	// ----------------------------------------------------
	
	/**
	 * JSセット
	 * @param string $path
	 */
	public function set_js( $path )
	{
		Yii::app()->clientScript->registerScriptFile( $this->getAssetsUrl() . '/' . $path );
	}
	
	// ----------------------------------------------------
	
	/**
	 * スクリプト追加
	 * @param string $js
	 * @param int $position
	 */
	public function add_js( $js, $postion )
	{
		Yii::app()->clientScript->registerScript(
			'1',
			$js,
			$postion
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * アセットのパスを取得
	 */
	protected function getAssetsUrl()
	{
		if ($this->_assetsUrl !== null)
		{
			return $this->_assetsUrl;
		}
		else	
		{
			$assetsPath = Yii::getPathOfAlias('application.' . $this->asset_path );
			
			if (YII_DEBUG)
			{
				$assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, true);
			}
			else
			{
				$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
			}
			return $this->_assetsUrl = $assetsUrl;
		}
	}
	
	// ----------------------------------------------------
	
}