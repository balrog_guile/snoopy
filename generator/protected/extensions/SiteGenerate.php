<?php
/* =============================================================================
 * サイトジェネレートクラス
 * ========================================================================== */
require_once 'markdown.php';
require_once 'Smarty.class.php';
class SiteGenerate
{
	//smartyオブジェクト
	protected $smarty;
	
	// ----------------------------------------------------
	
	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		$this->smarty = new smarty();
		$config = Yii::app()->params['smarty'];
		$this->smarty->template_dir = $config['template_dir'];
		$this->smarty->compile_dir =  $config['compile_dir'];
		$this->smarty->plugins_dir[] = $config['plugin_dir'];
		$this->smarty->left_delimiter = $config['left_delimiter'];
		$this->smarty->right_delimiter = $config['right_delimiter'];
	}
	
	// ----------------------------------------------------
	
	/**
	 * 作成実行
	 * @param string $path
	 * @param array $data
	 */
	public function create( $path, $data )
	{
		
		
		
		
	}
	
	// ----------------------------------------------------
}