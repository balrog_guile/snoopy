<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
?>

<h1>サイト管理</h1>

<hr />

<?php if( $message != '' ): ?>
<div class="message"><?php echo $message; ?></div>
<?php endif; ?>


<?php if( $error_message != '' ): ?>
<div class="error_message"><?php echo $error_message; ?></div>
<?php endif; ?>

<h2>新規サイト作成</h2>
<div class="form">
<?php $form = $this->beginWidget(
		'CActiveForm',
		array(
			'id'=>'site-form',
			'enableAjaxValidation'=>true,
			'enableClientValidation'=>true,
			'focus'=>array($model,'name'),
			'action' => '/index.php/site/create',
			'clientOptions' => array(
				'validateOnSubmit' => true,//submitするときチェックよろしく。
			)
		)
	);
?>
	
	<?php echo $form->errorSummary($model); ?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name',array()); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<div class="row submit">
		<div class="row buttons">
			<?php echo CHtml::submitButton( '作成' ); ?>
		</div>
		
	</div>
<?php $this->endWidget(); ?>
</div>

<hr />

<h2>作成済みサイト</h2>
<table>
	<tr>
		<th width="15%">作成実行</th>
		<th>サイト名</th>
		<th width="15%">操作</th>
	</tr>
	<?php foreach( $dirs as $dir ): ?>
		<tr>
			<td>
				<?php echo CHtml::button('Generate!', array( 'data-id' => rawurlencode($dir), 'class' => 'generate_btn') ); ?>
			</td>
			<td>
				<?php echo $dir; ?>
			</td>
			<td>
				<?php echo CHtml::button('削除', array( 'data-id' => rawurlencode($dir), 'class' => 'delete_btn') ); ?>
			</td>
		</tr>
	<?php endforeach; ?>
</table>